#!/usr/bin/env python3
import subprocess
import unittest
from unittest import mock

import cikcikci.launch as cikcikci_launch

class CommandLineExecutionTest(unittest.TestCase):

    def setUp(self):
        self.mock_options = mock.Mock()
        self.mock_options.search = str('saltBea')
        self.mock_options.account = str('ferayebend')
        self.mock_options.grab_timeline = True
        self.mock_options.grab_friends = False
        self.mock_options.grab_followers = False
        self.mock_options.streaming = str('saltBea')

    def test_execute_without_options(self):
        # When no option is given, help is printed out
        process = subprocess.Popen(['python3','cikcikci/launch.py'],
                                              stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE)
        out, err = process.communicate()
        self.assertIn(b'usage',out.lower())

    def test_search_option_execution(self):
        result = cikcikci_launch.main(self.mock_options)
        self.assertIn('searching', result)
