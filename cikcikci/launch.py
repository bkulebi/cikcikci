#!/usr/bin/python3
#import sys
#import os

from optparse import OptionParser

def main(options):
    if options.search:
        return 'searching %s'%options.search

if __name__ == "__main__":    
    usage = "usage: %prog [options]"
    parser = OptionParser(usage=usage)
    parser.add_option("-s",
                      "--search",
                      dest="search",
                      default=None,
                      help="search keyword to be scraped",
                      type="string")
    (options, args) = parser.parse_args()
   
    # If no option value is given from the commandline
    # print help
    if not options.search:
        parser.print_help()

    main(options)
